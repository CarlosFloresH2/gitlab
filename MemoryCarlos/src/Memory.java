// Carlos Flores Herrero
import java.util.Scanner;
	
public class Memory {
	public static Scanner reader = new Scanner(System.in);
	public static int medida = 4;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        //esto no es un comentario desde internet me cago en dios
		joc();

	}

	static char[][] inicialitzaTauler() {
		System.out.println("Bienvenido al juego del Memory");
		System.out.println("------------------------------\n");
		char[][] tabla = new char[medida][medida];
		for (int i = 0; i < medida; i++) {
			for (int j = 0; j < medida; j++) {
				tabla[i][j] = '?';
			}
		}
		return tabla;
	}
	
	static void joc() {
		char tauler[][] = new char[medida][medida];
		char secret[][] = new char[medida][medida];
		tauler = inicialitzaTauler();
		mostraTauler(tauler);

		secret = inicialitzaSecret(secret);

		torn(tauler, secret);

	}

	static void mostraTauler(char[][] tablero) {
		int k = 0;
		int r = 1;
		for (int f = 0; f < medida + 1; f++) {
			if (f > 0) {
				System.out.print(r+ " ");
				r++;
			} else {
				System.out.print("  ");
			}
		}
		System.out.println();
		for (int i = 0; i < medida; i++) {
			k++;
			System.out.print(k+ " ");
			for (int j = 0; j < medida; j++) {
				System.out.print(tablero[i][j]+" ");
			}
			System.out.println();
		}
	}

	static char[][] posaPeces() {
		char[][] relleno = new char[medida][medida];
		char letra = 'A';
		for (int i = 0; i < medida; i++) {
			for (int j = 0; j < medida; j++) {
				relleno[i][j] = letra;
				if (j % 2 != 0) {
					letra++;
				}
			}
		}
		return relleno;
	}

	static char[][] inicialitzaSecret(char[][] secreto) {
		secreto = posaPeces();
		remenaPeces(secreto);
		return secreto;
	}

	static char[][] remenaPeces(char[][] secreto) {
		char aux = 0;
		for (int i = 0; i < medida; i++) {
			for (int j = 0; j < medida; j++) {
				int rand1 = (int) (Math.random() * medida);
				int rand2 = (int) (Math.random() * medida);
				aux = secreto[i][j];
				secreto[i][j] = secreto[rand1][rand2];
				secreto[rand1][rand2] = aux;
			}
		}
		return secreto;
	}
	static boolean validaCasella(char[][] tabla, int x, int y) {
		boolean bien = false;
		if (tabla[x][y] == '?') {
			bien = true;
		} else {
			System.out.println("Casilla ya destapada...");
		}
		return bien;
	}
	static int validaDada(int verdad) {
		boolean correcto = false;
		while (correcto != true) {
			if (verdad >= 0 && verdad < medida) {
				correcto = true;
			} else {
				System.out.println("Introduce un nmero correcto: ");
				verdad = reader.nextInt();
				verdad = verdad - 1;
			}
		}
		return verdad;
	}
	static void torn(char[][] tablero, char[][] secreto) {
		int posicionx = 0;
		int posiciony = 0;
		int posicionx2 = 0;
		int posiciony2 = 0;
		int contadorA = 0;
		int contadorB = 0;
		boolean correcto = false;
		boolean fin = false;
		boolean fallo1 = true;
		boolean fallo2 = false;
		int n = 0;
		while (fin != true) {
			while (fallo1 != false && fin != true) {
				n++;
				System.out.println("\nJuagdor 1");
				System.out.println(n + " tirada...");
				while (correcto != true) {
					System.out.println("Introduce fila:");
					posicionx = reader.nextInt();
					posicionx = posicionx - 1;
					posicionx = validaDada(posicionx);
					System.out.println("Introduce columna: ");
					posiciony = reader.nextInt();
					posiciony = posiciony - 1;
					posiciony = validaDada(posiciony);
					correcto = validaCasella(tablero, posicionx, posiciony);
				}
				correcto = false;
				tablero[posicionx][posiciony] = secreto[posicionx][posiciony];
				mostraTauler(tablero);		
				if (n < 2) {
					posicionx2 = posicionx;
					posiciony2 = posiciony;
				} else {
					if (tablero[posicionx][posiciony] == tablero[posicionx2][posiciony2]) {
						System.out.println("Son iguales... ");
						contadorA++;
						n = 0;
						System.out.println("\nJugador 1 = "+ contadorA +" puntos.");
						System.out.println("Jugador 2 = "+ contadorB +" puntos.");
					} else {
						System.out.println("No son iguales...");
						tablero[posicionx][posiciony] = '?';
						tablero[posicionx2][posiciony2] = '?';
						n = 0;
						fallo1 = false;
						fallo2 = true;
						System.out.println("\nJugador 1 = "+ contadorA +" puntos.");
						System.out.println("Jugador 2 = "+ contadorB +" puntos.");
					}
				}
				if (contadorA + contadorB == (medida*2)) {
					fin = true;
				}
			}
			System.out.println();
			mostraTauler(tablero);
			while (fallo2 != false && fin != true) {
				n++;
				
				System.out.println("\nJuagdor 2");
				System.out.println(n + " tirada...");
				while (correcto != true) {
					System.out.println("Introduce fila:");
					posicionx = reader.nextInt();
					posicionx = posicionx - 1;
					posicionx = validaDada(posicionx);
					System.out.println("Introduce columna: ");
					posiciony = reader.nextInt();
					posiciony = posiciony - 1;
					posiciony = validaDada(posiciony);
					correcto = validaCasella(tablero, posicionx, posiciony);
				}
				correcto = false;
				tablero[posicionx][posiciony] = secreto[posicionx][posiciony];
				mostraTauler(tablero);
				if (n < 2) {
					posicionx2 = posicionx;
					posiciony2 = posiciony;
				} else {
					if (tablero[posicionx][posiciony] == tablero[posicionx2][posiciony2]) {
						System.out.println("Son iguales... ");
						contadorB++;
						n = 0;
						System.out.println("\nJugador 1 = "+ contadorA +" puntos.");
						System.out.println("Jugador 2 = "+ contadorB +" puntos.");
					} else {
						System.out.println("No son iguales...");
						tablero[posicionx][posiciony] = '?';
						tablero[posicionx2][posiciony2] = '?';
						n = 0;
						fallo1 = true;
						fallo2 = false;
						System.out.println("\nJugador 1 = "+ contadorA +" puntos.");
						System.out.println("Jugador 2 = "+ contadorB +" puntos.");
					}
				}
				if (contadorA + contadorB == (medida*medida/2)) {
					fin = true;
				}
				
			}
			System.out.println();
			mostraTauler(tablero);						
		}
		System.out.println("\nFin del Juego\n");
		if(contadorA > contadorB) {
			System.out.println("Gana el Jugador 1 con "+ contadorA+" puntos.");
			System.out.println("El Jugador 2 a conseguido "+ contadorB+" puntos.");
		}else if (contadorA < contadorB) {
			System.out.println("Gana el Jugador 2 con "+ contadorB+" puntos.");
			System.out.println("El Jugador 1 a conseguido "+ contadorA+" puntos.");
		}else
			System.out.println("Empate con "+ contadorA +" puntos");
	}

}

