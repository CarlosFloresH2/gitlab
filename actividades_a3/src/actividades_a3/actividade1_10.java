package actividades_a3;

import java.util.Scanner;

public class actividade1_10 {
	public static void main(String[] args) {
		//TODO Auto-generated method stub
		
		Scanner reader = new Scanner(System.in); 
		int a, b;
			
		System.out.print("Di un numero: ");
		a = reader.nextInt();
		
		System.out.print("Di otro numero: ");
		b = reader.nextInt();
		
		if (a > 0 & b > 0 & a != b) {
			if (a > b & a % b == 0) {
				
				System.out.println(a+ " Es multiplo de " +b);
			}
			
			else if (a > b & a % b != 0){
				
				System.out.println(a+ " No es multiplo de " +b);
			}
			
			else if (a < b & b % a == 0) {
				
				System.out.println(b+ " Es multiplo de " +a);
				
			}
			
			else if (a < b & b % a != 0) {
				
				System.out.println(b+ " No es multiplo de " +a);
			}
		}
		
		else {
			
			System.out.println("Alguna condicion no se cumple");
			
		}
								
		reader.close();
	}

}