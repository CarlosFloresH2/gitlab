//1.15.- Algorisme que llegeix dos n�meros en dos variables i intercanvia el seu valor. 

package actividades_a3;

import java.util.Scanner;

public class actividad1_15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner reader = new Scanner(System.in);
		int a, b, c;
		
		System.out.println("Introduce la primera variable: ");
		a = reader.nextInt();
		
		System.out.println("Introduce la segunda variable: ");
		b = reader.nextInt();
		
		c = a;
		a = b;
		b = c;
		
		System.out.println("Variables: a = "+ a +" b = "+ b);

	
	reader.close();
	}

}
