//1.14.- Algorisme que llegeix una quantitat per teclat i si �s superior a 500 li fa el
//10% de descompte, en cas contrari �nicament li fa el 5%.
//El resultat cal que ens digui la quantitat descomptada i la quantitat a pagar.

package actividades_a3;

import java.util.Scanner;

public class actividad1_14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner reader = new Scanner(System.in);
		float a;
		
		System.out.println("Ingresa el precio: ");
		a = reader.nextFloat();
		
		if (a > 500) {
			
			System.out.println("Tienes un descuento del 10 %, el descuento es de: "+ a * 0.1 +"�. "+ "El precio final de su producto es de: " + (a - a * 0.1) + "�");
			
		}else{
			System.out.println("Tienes un descuento del 5 %, el descuento es de: "+ a * 0.05 +"�. "+ "El precio final de su producto es de: " + (a - a * 0.05) + "�");
						
		}
		
		reader.close();

	}

}
