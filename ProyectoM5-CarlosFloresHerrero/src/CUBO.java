
/**
 * Declarem el Scanner per poder llegir el que introdueixi l'usuari.
 */
import java.util.Scanner;

/**
 * CUBO
 * 
 * @version 1
 * @author Carlos Flores Herrero
 * 
 */
public class CUBO {

	/**
	 * És on es troba tot el codi
	 * 
	 * @param args Main
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		int opcion = 0;
		int k = 0;
		boolean done = false;
		while (done != true) {
			opcion = reader.nextInt();
			if (opcion == 5) {
				done = true;
			} else {
				if (k == 0) {
					error1(opcion);
					k++;
				} else if (k == 1) {
					error2(opcion);
					k++;
				} else {
					error3(opcion);
					k = 0;
				}
			}
		}
		reader.close();
		opcionCorrecta(opcion);
	}

	/**
	 * Primera funció que mostrará cuan el número introduit sigui incorrecte
	 * 
	 * @see Main
	 * @param opcion Número introduit per l'usuari que defineix les cares del cub
	 */
	static void error1(int opcion) {
		System.out.println("El cub és de DAM-C...");
	}

	/**
	 * Segona funció que mostrará cuan el número introduit sigui incorrecte
	 * 
	 * @see Main
	 * @param opcion Número introduit per l'usuari que defineix les cares del cub
	 */
	static void error2(int opcion) {
		System.out.println("El cub és nostre...");
	}

	/**
	 * Tercera funció que mostrará cuan el número introduit sigui incorrecte, una
	 * vegada arribat aquí, es reinicia la variable pero tornar a l'error3
	 *
	 * @see Main
	 * @param opcion Número introduit per l'usuari que defineix les cares del cub
	 */
	static void error3(int opcion) {
		System.out.println("Intrús...");
	}

	/**
	 * Funció que moestra el resultat de haber encertat el número de cares
	 *
	 * @see Main
	 * @param opcion Número introduit per l'usuari que defineix les cares del cub
	 */
	static void opcionCorrecta(int opcion) {
		System.out.println("\t   +--------+\r\n" + "\t  /        /|\r\n" + "\t /        / |\r\n" + "\t+--------+  |\r\n"
				+ "\t|        |  +\r\n" + "\t|        | /\r\n" + "\t|        |/\r\n" + "\t+--------+");
		System.out.println("Per fi reclamem el que és nostre");
		System.out.println("\tOle DAM-C");

	}

}
