import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CUBOTest{
	
	@Test
	void testerror1() {
		int opcion = 2;
		int k = 0;
		if (opcion != 5 && k == 0) {
			prueba1(opcion, k);
		} else {
			fail("Error");
		}
	}

	static void prueba1(int opcion, int k) {
		String prueba1 = "El cubo es de DAM-C...";
		assertEquals("El cubo es de DAM-C...", prueba1);
	}

	@Test
	void testerror2() {
		int opcion = 2;
		int k = 1;
		if (opcion != 5 && k == 1) {
			prueba2(opcion, k);
		} else {
			fail("Error");
		}
	}
	static void prueba2(int opcion, int k) {
		String prueba1 = "Intruso...";
		assertEquals("Intruso...", prueba1);
	}
	@Test
	void testerror3() {
		int opcion = 2;
		int k = 1;
		if (opcion != 5 && k == 1) {
			prueba3(opcion, k);
		} else {
			fail("Error");
		}
	}

	static void prueba3(int opcion, int k) {
		String prueba1 = "Intruso...";
		assertEquals("Intruso...", prueba1);
	}

	@Test
	void testPruebaCorrecta() {
		int opcion = 5;
		if (opcion == 5) {
			pruebaCorrecta(opcion);
		} else {
			fail("opcion != 5");
		}
	}

	static void pruebaCorrecta(int opcion) {
		String resultado ="\t   +--------+\r\n" + 
			    "\t  /        /|\r\n" + 
				"\t /        / |\r\n" + 
				"\t+--------+  |\r\n" + 
				"\t|        |  +\r\n" + 
				"\t|        | /\r\n" + 
				"\t|        |/\r\n" + 
				"\t+--------+";
		assertEquals("\t   +--------+\r\n" + 
			    "\t  /        /|\r\n" + 
				"\t /        / |\r\n" + 
				"\t+--------+  |\r\n" + 
				"\t|        |  +\r\n" + 
				"\t|        | /\r\n" + 
				"\t|        |/\r\n" + 
				"\t+--------+", resultado);
	}

}

